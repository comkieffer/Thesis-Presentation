# Thesis Presentation

Presentation for my Master's Thesis. The Report for the thesis can be found [here](http://comkieffer.gitlab.io/Thesis-Report/thesis.pdf) and the code can be found [here](https://gitlab.com/comkieffer/Thesis-Code/)

An up to date version of this presentation can be seen [here](http://comkieffer.gitlab.io/Thesis-Presentation/presentation.pdf)

## Useful Advice

Somebody once said to me: 

> "Tell them what you're going to tell them,
>  Tell them,
>  Tell them what you told them"

It turns out that it is pretty good advice on how to structure your presentation. 
