\documentclass{beamer}
\usepackage{polimi_thesis}

\newcommand\blocktitle[1]{%
  \par\bigskip
  {\centering\large\bfseries#1}\smallskip}

\usetheme{polimix}

\setlength{\abovedisplayskip}{0.25em}
\setlength{\belowdisplayskip}{0.75em}

\AtBeginSection[]{
  \begingroup
    \setbeamerfont{section title}{size=\Huge}
    \setbeamercolor{section title}{fg=white}

    \setbeamertemplate{background}{
      %edit this tikzpicture to customize the size and colors of the background rectangles
      \begin{tikzpicture}
          \useasboundingbox (0,0) rectangle(\the\paperwidth,\the\paperheight);
          \fill[color=poliblue3] (0,0) rectangle(\the\paperwidth,\the\paperheight);     
      \end{tikzpicture}
    }

  \begin{frame}
    \vfill
    \centering
      \begingroup
      \begin{beamercolorbox}[sep=12pt,center]{section title}
          \usebeamerfont{section title}\insertsection\par
      \end{beamercolorbox}
      \endgroup
      \vfill
  \end{frame}

  \endgroup
}



\title{Data-Driven Attitude Control Design For Multirotor UAVs}

\author[author]{Thibaud Chupin}
%\supervisor{Supervisor}{Prof. Marco LOVERA}

% No co-advisors since the template only supports 1
% \coadvisor{Co-Advisor}{Ing. Pietro Panizza}
% \coadvisor{Co-Advisor}{Ing. Davide INVERNIZZI}
% \coadvisor{Co-Advisor}{Ing. Mattia GIURATO}
\date[28/04/2017]{28 Apr. 2016}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\addtocounter{framenumber}{-1}

\begin{frame}
  \frametitle{Index}
  \tableofcontents
\end{frame}


\section{Motivation \& Problem Statement}

\begin{frame}
  \frametitle{The Quadrotor}
    
  \begin{columns}
    \begin{column}{0.5\textwidth}
       \includegraphics[width=\textwidth]{quadrotor.png}
    \end{column}
    \begin{column}{0.5\textwidth} 
      Vehicle Characteristics:
      \begin{itemize}
        \item 4 Rotors
        \item Arm Length: \num{500} \si{\milli\metre} 
        \item Weight: \num{1.5}\si{\kilo\gram}
      \end{itemize}
    \end{column}
  \end{columns}

  \vspace{2em}
  \centering
  All the experiments were performed on a test-bed constraining all DoFs except pitch rotation.
\end{frame}

\begin{frame}
  \frametitle{Structure Of The Pitch Angle Controller}
  
  \begin{figure}
    \centering
    \hspace{-0.5cm}
    \input{Figures/pitch_ctrl_loop.tikz}
  \end{figure}

  \begin{columns}
    \begin{column}{0.5\textwidth}
      \( \pitchangle^o \) Pitch Angle Set Point \\
      \( \pitchangle \) Measured Pitch Angle \\
      \vspace{1em}

      \( q^o \) Pitch Rate Set Point \\
      \( q \) Measured Pitch Rate \\
      \vspace{1em}

      \( u \) Control Variable
    \end{column}
    \begin{column}{0.5\textwidth} 
      Tunable control parameters:

      \begin{itemize}
        \item PID: \( K_{p_i} \), \( K_{i_i} \), \( K_{d_i} \)
        \item PD: \( K_{p_o} \), \( K_{d_o} \)
      \end{itemize}

      Fixed parameters:

      \begin{itemize}
        \item Sample Time (\( T_s \))
        \item Filter Time Constants (\( T_{f_i}, T_{f_o} \)) 
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Attitude Controller Tuning --- State of the Art}

  \begin{columns}
    \begin{column}{0.5\textwidth}
      \blocktitle{Manual Tuning}
      \begin{itemize}
        \item Time Consuming
        \item Trial \& Error
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth} 
      \blocktitle{Model-based Tuning}
      \begin{itemize}
        \item High performance
        \item Requires accurate \& reliable mathematical models
      \end{itemize}
    \end{column}
  \end{columns}

  \blocktitle{Data-driven Tuning}
  \vspace{-0.25cm}
  \begin{itemize}
    \item Tuning is performed directly from the experimental input-output data
    \item Does not require mathematical models
    \item Does not require specialised knowledge about the plant
    \item Allows for fast retuning
  \end{itemize}

\end{frame}


\section{The Virtual Reference Feedback Tuning (VRFT) Approach}

\begin{frame}
  \frametitle{The VRFT Approach}

  \begin{centering}
    \large
    VRFT is a \emph{data-driven}, \emph{model-reference} method for \emph{structured} controller synthesis. 
  \end{centering}

  \begin{itemize}
    \item<2-> \emph{data-driven} --- Relies on a set of input-output data instead of a mathematical plant model 
    \item<3-> \emph{model-reference} --- Minimises the difference between a reference model and the actual closed loop transfer function
    \item<4-> \emph{structured} --- Finds a controller within a pre-specified controller family
  \end{itemize}

\end{frame}

\begin{frame}[t]
  \frametitle{The VRFT Approach --- Problem Formulation}

  The control objective is the minimisation of 

  \begin{equation*}
    J_{MR}(\theta) = \left \Vert 
        \left( M_R(z) - \frac{P(z)C(z;\ \theta)}{1 + P(z)C(z;\ \theta)} \right) W(z) 
      \right \Vert_2^2
  \end{equation*}

  \only<1>{
  \vspace{1em}
  Where 
  \begin{itemize}
    \item \( M_R(z) \) is the chosen reference model
    \item \( P(z) \) is the plant model
    \item \( C(z;\ \theta) \) is the parametrised controller
    \item \( W(z) \) is a frequency weighting function
  \end{itemize}
  }

  \only<2->{
    VRFT provides a solution to this problem 

    \begin{equation*}
      J_{VR}(\theta) = E \left[ \left( u_L(t) - C_i(z;\ \theta) \ e_{iL}(t) \right)^2 \right]
    \end{equation*}
  }

  \only<3>{
    \vspace{2em}
    \centering
    With a suitable choice of a pre-filter \( L(z) \) applied to the data it can be shown that the criteria are equal.
  }

  \only<4>{
    \begin{equation*}
      \left| L(\freqd) \right|^2 = \left| 
        \left(1 - M_R(\freqd) \right) M_R(\freqd) W(\freqd) 
      \right|^2 \frac{1}{\Phi_u(\omega)}, \quad \forall \omega \in \left[ -\pi,\ \pi \right]
    \end{equation*}
  }
\end{frame}

\begin{frame}
  \frametitle{Prerequisites For VRFT}

  Given: 
  \begin{itemize}
    \item A set of open-loop input-output data where the input is persistently exciting 
    \begin{align*}
      u &= \left\{ u_0,\ u_1,\ u_2,\ \ldots\ , u_n \right\} \\
      y &= \left\{ y_0,\ y_1,\ y_2,\ \ldots\ , y_n \right\}
    \end{align*}

    \item A reference model \( M_R(z)\) describing the desired behaviour of the closed loop system
    \item A controller family \( \left\{ C(z;\ \theta) \right\} \) comprising only controllers that can be expressed as a linear combination of linear, discrete-time, transfer functions
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{The VRFT Control Scheme}
  
  \begin{figure}
    \centering
    \input{Figures/vrft.tikz}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{The VRFT Procedure}

  \begin{enumerate}
    \item Compute the \emph{virtual reference} signal
    \begin{equation*}
      r_v(t) = M^{-1}(z)y(t)
    \end{equation*} 

    \item Compute the controller input
    \begin{equation*}
      e(t) = r_v(t) - y(t)
    \end{equation*}

    \item Filter the signals \( u(t) \) and \( e(t) \) with a suitable filter \( L(z) \)
    \begin{equation*}
      u_L(t) = L(z) u(t) \qquad e_L(t) = L(z) e(t)
    \end{equation*}
  \end{enumerate}
  
\end{frame}

\begin{frame}
  \frametitle{The VRFT Result}

  The optimal parameter vector \( \hat{\theta} \) is
  \begin{equation*}
    \hat{\theta} = \argmin_{\theta}  
      \frac{1}{N} \Sum{\left[ u_L(t) - C(z;\ \theta) \ e_{L}(t) \right]^2}{t, 1, N}
  \end{equation*}
\end{frame}


\section{VRFT \& Cascade Control Schemes}

\begin{frame}
  \frametitle{VRFT Control Scheme for cascade control systems}

  \begin{figure}
    \centering
    \input{Figures/vrft_two_dof.tikz}
  \end{figure}

  \centering
  The controllers \( C_o \) and \( C_i \) can be tuned with a single set of data!

\end{frame}

\begin{frame}
  \frametitle{Cascade VRFT Prerequisites}

  Given: 
  \begin{itemize}
    \item A set of open-loop input-output data where the input is persistently exciting 
    \begin{align*}
          u &= \left\{ u_0,\ u_1,\ u_2,\ \ldots\ , u_n \right\} \\
        y_i &= \left\{ y_{i_0},\ y_{i_1},\ y_{i_2},\ \ldots\ , y_{i_n} \right\} \\
        y_o &= \left\{ y_{o_0},\ y_{o_1},\ y_{o_2},\ \ldots\ , y_{o_n} \right\}
    \end{align*}

    \item An inner reference model \( M_{R_i}(z) \) and an outer reference model \( M_{R_o}(z) \) describing the desired behaviour of the closed loop systems
    \item A controller families \( \left\{ C_i(z;\ \theta_i) \right\} \) and \( \left\{ C_o(z;\ \theta_o) \right\} \) comprising only controllers that can be expressed as a linear combination of linear, discrete-time, transfer functions
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Tuning The Inner Controller}

  The method is the same used for the standard case using:
  \begin{itemize}
    \item The experimental data
    \begin{align*}
          u &= \left\{ u_0,\ u_1,\ u_2,\ \ldots\ , u_n \right\} \\
        y_i &= \left\{ y_{i_0},\ y_{i_1},\ y_{i_2},\ \ldots\ , y_{i_n} \right\} \\
    \end{align*}

    \vspace{-0.85cm}
    \item The reference model \( M_{R_i}(z) \)
  \end{itemize}

  \vspace{0.5cm}
  The optimal parameter vector \( \hat{\theta}_i \) is
  \begin{equation*}
    \hat{\theta}_i = \argmin_{\theta_i}
      \frac{1}{N} \Sum{\left[ u_L(t) - C_i(z;\ \theta) \ e_{iL}(t) \right]^2}{t, 1, N}
  \end{equation*}

\end{frame}

\begin{frame}[t]
  \frametitle{Tuning the outer controller}
  \vspace{1em}

  \centering
  Consider an extended \emph{plant} comprising the entire dotted area

  \only<1>{
  \begin{figure}
    \centering
    \input{Figures/vrft_two_dof_inner_highlight.tikz}
  \end{figure}
  }

  \only<2>{
  \begin{figure}
    \centering
    \input{Figures/vrft_two_dof_collapsed.tikz}
  \end{figure}
  }

\end{frame}

\begin{frame}
  \frametitle{What is the input-output dataset}

  We need an input-output dataset for this new \emph{plant}.
  \begin{itemize}
    \item Input \( r_i(t) \), Unknown
    \item Output \( y_o(t) \), Measured
  \end{itemize}

  \pause
  \vspace{2em}
  Compute the reference input \( r_i(t) \)
  \begin{equation*}
    r_i(t) = C_i(z;\ \theta)^{-1} u(t) + y_i(t)
  \end{equation*}

\end{frame}

\begin{frame}
  \frametitle{Outer Loop Controller Tuning}

  Apply VRFT using:

  \begin{itemize}
    \item The experimental data
    \begin{align*}
        r_i &= \left\{ r_0,\ r_1,\ r_2,\ \ldots\ , r_n \right\} \\
        y_o &= \left\{ y_{o_1},\ y_{o_2},\ \ldots\ , y_{o_n} \right\} \\
    \end{align*}

    \vspace{-0.85cm}
    \item The reference model \( M_{R_o}(z) \)
  \end{itemize}

  \vspace{0.5cm}
  The optimal parameter vector \( \hat{\theta}_o \) is
  \begin{equation*}
    \hat{\theta}_o = \argmin_{\theta_o} 
      \frac{1}{N} \Sum{\left[ r_i(t) - C_o(z;\ \theta) \ e_{oL}(t) \right]^2}{t, 1, N}
  \end{equation*}
    
\end{frame}

\section{Simulation Results}

\begin{frame}
  \frametitle{Simulations, Why?}

  \begingroup
    \centering
    Why use simulations when VRFT is a data-driven method and does not require a plant model?
  \endgroup
  \vspace{2em}

  \begin{itemize}
    \item<2-> An identified model was available from previous work
    \begin{equation*}
      P(s) = \frac{0.423}{s + 1.33}
    \end{equation*}

    \item<3-> Experiment with different controllers without taking the risk of damaging the system
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Required Steps}

  To use VRFT to tune the controllers on the simulated system we must: 

  \begin{enumerate}
    \item<1-> \Large Simulate a set of input-output data
    \item<2-> \Large Decide the controller families to use for the inner and outer loops
    \item<3-> \Large Decide the reference models for the inner and outer loops
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Simulated Input-Output Data}

  \vspace{-0.25cm}
  \begin{figure}[htbp]
    \centering
    \input{Figures/pitch_ctrl_loop_io_signals.tikz}
  \end{figure}

  \begin{itemize}
    \item A Pseudo Random Binary Sequence was generated and fed as an input (\( \delta M \)) to the identified model in open-loop conditions
    \item The simulated output, the pitch rate (\( q \)), was logged
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Simulated Input-Output Data}

  \vspace{-0.25cm}
  \begin{figure}[htbp]
    \centering
    \input{Figures/Plots/plant_model_sim.tikz}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{Controller Families}

  \begin{itemize}
    \item Inner Controller --- \( PID \)
    \begin{equation*}
      PID(s) = K_{p_i} + \frac{K_{i_i}}{s} + \frac{K_{d_i}s}{T_fs + 1}
    \end{equation*}

    \item Outer Controller --- \( PD \)
    \begin{equation*}      
      PD(s) = K_{p_i} + \frac{K_{d_o}s}{T_fs + 1} 
    \end{equation*}
  \end{itemize}

  \centering
  The controller families were fixed by the architecture of the controller. 

\end{frame}

\begin{frame}
  \frametitle{Reference Models}

  \begin{itemize}
    \item Inner Reference Model
    \begin{equation*}
      M_i(s) = 
        \frac{\omega_{n_i}^2}{s^2 + 2 \zeta_i \omega_{n_i} s + \omega_{n_i}^2} 
        \frac{s + z_0}{z_0}
    \end{equation*}

    \item Outer Reference Model
    \begin{equation*}
      M_o(s) = \frac{\omega_{n_o}^2}{s^2 + 2 \zeta_o \omega_{n_o} s + \omega_{n_o}^2}
    \end{equation*}  
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Simulations}

  Several simulations were performed to identify a range of possible controllers two of which will be discussed here:  

  \begin{itemize}
    \item Conservative reference models --- similar to existing system performance
    \begin{equation*}
        \omega_{n_i} = \num{8} \si{\radian\per\second} \quad \zeta_i = \num{0.9} \quad 
        \omega_{n_0} = \num{4} \si{\radian\per\second} \quad \zeta_o = \num{0.9} 
    \end{equation*}

    \item High bandwidth reference models
    \begin{equation*}
        \omega_{n_i} = \num{15} \si{\radian\per\second} \quad \zeta_i = 0.9 \quad 
        \omega_{n_o} = \num{10} \si{\radian\per\second} \quad \zeta_0 = 0.9 
    \end{equation*}

  \end{itemize}

  \centering
  In each experiment the simulated pitch angle and control effort were logged.

\end{frame}

\begin{frame}
  \frametitle{Simulation Results --- Conservative Hypothesis}

  \only<1>{
    \centering
    \vspace{0.5cm}
    \input{Figures/Plots/simres_bode_innerref_norm.tikz}
  }

  \only<2>{
    \centering
    \vspace{0.5cm}   
    \input{Figures/Plots/simres_bode_outerref_norm.tikz}
  }

  \only<3>{
    \centering
    \vspace{0.5cm}        
    \input{Figures/Plots/simres_norm.tikz}
  }

\end{frame}

\begin{frame}
  \frametitle{Simulation Results --- High Bandwidth Reference Models}

  \only<1>{
    \centering
    \vspace{0.5cm}
    \input{Figures/Plots/simres_hi.tikz}
  }

\end{frame}

\begin{frame}
  \frametitle{All Models Are Wrong ...}

  \begin{quote}
    All models are wrong but some are useful
    \flushright{
      \textit{--- George Box}
    }
  \end{quote}

  \vspace{1cm}
  The identified model does not account for:
  \begin{itemize}
    \item Saturations
    \item Actuator Dynamics
  \end{itemize}

\end{frame}



\section{Experimental Results}

\begin{frame}
  \frametitle{Required Steps}

  To use VRFT to tune the controllers on the real system we must: 

  \begin{enumerate}
    \item<1-> Acquire a set of input-output data
    \item<2-> Decide the reference models for the inner and outer loops
  \end{enumerate}

  \centering
  \vspace{0.75cm}
  \onslide<3>{The controller families were set during the simulation phase}
\end{frame}


\begin{frame}
  \frametitle{Input-Output Data --- Open-loop experiment}

  \begin{figure}[htbp]
    \centering
    \input{Figures/Plots/vrft_io_data.tikz}
  \end{figure}
  
\end{frame}

\begin{frame}
  \frametitle{Reference Models}

  \begin{itemize}
    \item Inner Reference Model (\(\omega_{n_i} = \) \num{8}\si{\radian\per\second}, \( \zeta = \) \num{0.9})
    \begin{equation*}
      M_{R_i}(s) = \frac{64s + 320}{5s^2 + 72s + 320} \frac{s - 5}{5}
    \end{equation*}

    \item Outer Reference Model (\(\omega_{n_i} = \) \num{4}\si{\radian\per\second}, \( \zeta = \) \num{0.9})
    \begin{equation*}
      M_{Ro}(z) = \frac{7.81z + 7.625}{z^2 - 1.929z + 0.9305}10^{-4}
    \end{equation*}
  \end{itemize}
  
  \vspace{2em}
  \centering  
  The above reference models were settled upon at the conclusion of the testing campaign. These were deemed to provide the best performance.
\end{frame}

\begin{frame}
  \frametitle{Controller Performance}

  The following controllers were synthesised 

  \begin{itemize}
    \item PID --- \( K_{p_i} = 0.2978\), \( K_{i_i} = 0.514\), \( K_{d_i} = 0.0\)
    \item PD --- \( K_{p_o} = 1.6057\), \( K_{d_o} = 0.0\)
  \end{itemize}

  \vspace{1em}
  The performance of these controllers was compared to that of pre-existing \hinf tuned controllers.

  \begin{itemize}
    \item PID --- \( K_{p_i} = 0.298\), \( K_{i_i} = 0.304\), \( K_{d_i} = 0.0499\)
    \item PD --- \( K_{p_o} = 2.0\), \( K_{d_o} = 0.0522\)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Experimental Results --- Set-point tracking}

      \centering
      \input{Figures/Plots/pitch_set_point_tracking_vrft.tikz}

\end{frame}

\begin{frame}
  \frametitle{Experimental Results - Disturbance rejection}

  \centering
  \input{Figures/Plots/disturbed_control_vrft.tikz}

\end{frame}

\section{Conclusions}

\begin{frame}
    \frametitle{Conclusions}

    Over the course of this thesis I
    \begin{itemize}
      \item<1-> Synthesised a controller for a quadrotor using VRFT 
      \item<2-> Validated the controller using simulations
      \item<3-> Validated the controller on the actual hardware
      \item<4-> Showed that the controller can achieve performance similar to model-based methods 
    \end{itemize}
\end{frame}

\end{document} 
