
\tikzsetnextfilename{simres_bode_innerref_hi}

\def\axisdefaultwidth{10cm}
\def\axisdefaultheight{5cm}

\begin{tikzpicture}
    \begin{axis}[
          name=top_plt 
        , height = 4.5cm, width=10cm,
        , xmode = log
        , xticklabels = {,,}
        , ylabel = Mag. (dB)
        , yticklabel style={
            /pgf/number format/fixed,
            /pgf/number format/precision=0
        }
        , xmin = 1, xmax = 100
        , no markers
        , xminorgrids, ymajorgrids
        , title = {Inner Loop}
        ]
        \addplot[dashdotted, blue] table[x=omega,y=mag] {Figures/Plots/simres_innerref_bode_hi.dat};
        \addplot                   table[x=omega,y=mag] {Figures/Plots/simres_innervrft_bode_hi.dat};
    \end{axis}

    \begin{axis}[
          at=(top_plt.below south west), anchor=above north west, node distance =? 0.25cm
        , height = 3.5cm, width = 10cm,
        , xmode = log
        , xlabel = $\omega (\si{\radian})$
        , ytick = {-90, 0}
        , ylabel = Phase (\si{\degree})
        , yticklabel style={
            /pgf/number format/fixed,
            /pgf/number format/precision=0
        }
        , xmin = 1, xmax = 100
        , legend style = { at={(0.5, -1.5cm) }, anchor = north} 
        , legend columns = {2}
        , no markers
        , xminorgrids, ymajorgrids
        ]
        \addplot[dashdotted, blue] table[x=omega,y=phase] {Figures/Plots/simres_innerref_bode_hi.dat};
        \addplot                   table[x=omega,y=phase] {Figures/Plots/simres_innervrft_bode_hi.dat};

        \legend{Reference Model, Closed Loop}
    \end{axis}

\end{tikzpicture}
