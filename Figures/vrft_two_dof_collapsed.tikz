

\tikzsetnextfilename{vrft_two_dof_collapsed}


\tikzstyle{hhilit} = [{draw=red, thick, densely dotted, inner xsep=2em, inner ysep=1em}]

\begin{tikzpicture}[auto, trim left, scale=0.8, every node/.style={transform shape}, >=latex]
    
    %
    % Draw the upper control scheme
    %
    
    % Draw the main control loop nodes
    \draw
    node[input] (input) {}
    node[hidden, right of = input        , node distance = 0.55cm] (oinputfork) {}
    node[sum   , right of = oinputfork   , node distance = 0.55cm] (osum) {}
    node[block , right of = osum         , node distance = 1.50cm] (ocontroller) {$C_o(z;\ \theta_i)$}
    node[hidden, right of = ocontroller  , node distance = 1.10cm] (iinputfork) {}
    node[sum   , right of = iinputfork   , node distance = 0.65cm] (isum) {}
    node[block , right of = isum         , node distance = 1.75cm] (icontroller) {$C_i(z;\ \theta_o)$}
    node[block , right of = icontroller  , node distance = 2.00cm] (iplant) {$P_i(z)$}
    node[hidden, right of = iplant       , node distance = 1.00cm] (ioutputfork) {}
    node[block , right of = ioutputfork  , node distance = 1.25cm] (oplant) {$P_o(z)$}
    node[hidden, right of = oplant       , node distance = 1.00cm] (ooutputfork) {}
    node[output, right of = ooutputfork  , node distance = 0.75cm] (output) {}

    node[hidden, below of = iplant                         ] (iretroaction) {}
    node[hidden, below of = iretroaction, node distance = 0.5cm] (oretroaction) {};
    
    \node[hhilit, fit = (isum) (iplant) (ioutputfork) (iretroaction) (oplant)] (comm) {};
    \node[above of = comm, node distance = 1.25cm] {Inner Loop};


    % Connect everything together
    \draw[->]       (input) node[left] {$r_o$} -- (oinputfork)  -- (osum);
    \draw[->]        (osum) -- (ocontroller);
    \draw[->] (ocontroller) -- (iinputfork)    -- (isum);
    \draw[->]        (isum) -- (icontroller);
    \draw[->] (icontroller) -- (iplant);
    \draw[->]      (iplant) -- (ioutputfork)   -- (oplant);
    \draw[->]      (oplant) -- (ooutputfork)   -- (output) node[right] {$y_o$};
    
    % Draw retroactions
    \draw[->] (ioutputfork) |- (iretroaction) -| (isum) node[pos=0.8] {$-$};
    \draw[->] (ooutputfork) |- (oretroaction) -| (osum) node[pos=0.9] {$-$};

    %
    % Draw the lower control scheme
    %


    % Draw the main control loop nodes
    \draw
    node[hidden, below of = input       , node distance = 4.00cm] (offset) {}
    node[input , right of = offset      , node distance = 1.55cm] (input_2) {}
    node[sum   , right of = input_2     , node distance = 1.60cm] (sum_2) {}
    node[block , right of = sum_2       , node distance = 2.00cm] (controller_2) {$C_o(z; \theta_o)$}
    node[block , right of = controller_2, node distance = 3.00cm] (plant_2) {$T_i(z) P_o(z)$}
    node[hidden, right of = plant_2     , node distance = 1.50cm] (outputfork_2) {}
    node[output, right of = outputfork_2, node distance = 1.30cm] (output_2) {}
    node[hidden, below of = plant_2                             ] (retroaction_2) {};
    
    % Connect everything together
    \draw[->]      (input_2) node[left] {$r_0$} -- (sum_2);
    \draw[->]        (sum_2) -- (controller_2) node[midway] {$e_o$};
    \draw[->] (controller_2) -- (plant_2) node[midway] {$r_i$};
    \draw[->]      (plant_2) -- (outputfork_2) -- (output_2) node[right] {$y_o$};
    \draw[->] (outputfork_2) |- (retroaction_2) -| node[pos=0.9] {$-$} (sum_2);

    \draw[-, dotted] (comm.south west) -- (plant_2.north west);
    \draw[-, dotted] (comm.south east) -- (plant_2.north east);
        
\end{tikzpicture}
    

